import edu.ntnu.jonathhl.idatt2001.Misc.Department;
import edu.ntnu.jonathhl.idatt2001.Misc.Hospital;
import edu.ntnu.jonathhl.idatt2001.Misc.HospitalTestData;
import edu.ntnu.jonathhl.idatt2001.Misc.RemoveException;
import edu.ntnu.jonathhl.idatt2001.Person.Employee;
import edu.ntnu.jonathhl.idatt2001.Person.Patient;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class containing JUnit tests for the class Department.java.
 * 
 * @author Jonathan Løseth
 * 
 * @version 1.0.1
 */
public class DepartmentTests {

    Department dm = new Department("Department");

    @Test
    @DisplayName("Checks if remove throws an exception if patient could not be found")
    public void throwsExceptionWhenRemoveFromSystemDoesntFindPatient() {
        assertThrows(RemoveException.class, () -> {
            dm.remove(new Patient("Del", "FinnesIkee", "2", ""));
        });
    }

    @Test
    @DisplayName("Checks if remove throws an exception if employee could not be found")
    public void throwsExceptionWhenRemoveFromSystemDoesntFindEmployee() {
        assertThrows(RemoveException.class, () -> {
            dm.remove(new Employee("Del", "FinnesIkee", "2"));
        });
    }

    @Test
    @DisplayName("Checks if remove works as intended on patients.")
    public void removePatientFromSystem() throws RemoveException {
        dm.addPatient(new Patient("Salti", "Kaffen", "3", ""));
        dm.remove(new Patient("Salti", "Kaffen", "3", ""));
        assertTrue(dm.getPatientRegister().size() == 0);
    }

    @Test
    @DisplayName("Checks if remove works as intended on employees.")
    public void removeEmployeeFromSystem() throws RemoveException {
        dm.addEmployee(new Employee("Salti", "Kaffen", "3"));
        dm.remove(new Employee("Salti", "Kaffen", "3"));
        assertTrue(dm.getEmployeeRegister().size() == 0);
    }

    @Test
    @DisplayName("Check if remove throws exception if an employee with the same name as a patient is insterted")
    public void removePatientNotEmployeeThrowsException() throws RemoveException {
        Hospital hospital = HospitalTestData.fillRegisterWithTestData(new Hospital("Sykehus"));
        Department dm = hospital.getDepartments().get(0);
        String fName = "Test";
        String lName = "Tester";
        String socialSecurityNumber = "11111";
        dm.addEmployee(new Employee(fName, lName, socialSecurityNumber));
        assertThrows(RemoveException.class, () -> dm.remove(new Patient(fName, lName, socialSecurityNumber, "")));
    }

    @Test
    @DisplayName("Check if remove throws exception if a patient with the same name as an employee is inserted")
    public void removeEmployeeNotPatientThrowsException() throws RemoveException {
        Hospital hospital = HospitalTestData.fillRegisterWithTestData(new Hospital("Sykehus"));
        Department department = hospital.getDepartments().get(0);
        String fName = "Test";
        String lName = "Tester";
        String socialSecurityNumber = "11111";
        dm.addPatient(new Patient(fName, lName, socialSecurityNumber, ""));
        assertThrows(RemoveException.class, () -> dm.remove(new Employee(fName, lName, socialSecurityNumber)));
    }
}
