package edu.ntnu.jonathhl.idatt2001.Misc;

import edu.ntnu.jonathhl.idatt2001.Person.Employee;
import edu.ntnu.jonathhl.idatt2001.Person.Patient;

/**
 * Simple client for the program
 * 
 * @author Jonathan Løseth
 */
public class HospitalClient {

    /**
     * Constructor for static usage.
     */
    public HospitalClient() {
    }

    public static void main(String[] args) {
        Hospital stOlav = new Hospital("St.Olavs Hospital");
        HospitalTestData.fillRegisterWithTestData(stOlav);

        Department department = stOlav.getDepartments().get(0);
        Employee removeEmployee = department.getEmployeeRegister().get(0);
        Patient removePatient = department.getPatientRegister().get(0);
        try {
            // Should work
            department.remove(removeEmployee);

            // Should not work, and throw an error message to the user.
            department.remove(new Patient("asdf", "wasd", "01010010", ""));
        } catch (RemoveException Re) {
            System.out.println("Person could not be removed. \n" + "Error: " + Re.getMessage());
        }
    }
}
