package edu.ntnu.jonathhl.idatt2001.Misc;

import edu.ntnu.jonathhl.idatt2001.Person.*;

/**
 * Class created for test data only.
 */
public final class HospitalTestData {

    /**
     * Constructor for static usage.
     */
    private HospitalTestData() {
    }

    /**
     * @author Task-creator; Professor
     *         Changes, fixes; Jonathan Løseth
     * 
     * @param hospital
     *            Hospital dummy data for testing.
     * 
     * @return Returns Dummies
     */
    public static Hospital fillRegisterWithTestData(final Hospital hospital) {

        Department emergency = new Department("Akutten");
        emergency.addEmployee(new Employee("Odd Even", "Primtallet", "1"));
        emergency.addEmployee(new Employee("Huppasahn", "DelFinito", "2"));
        emergency.addEmployee(new Employee("Rigmor", "Mortis", "3"));
        emergency.addEmployee(new GeneralPractitioner("Inco", "Gnito", "4"));
        emergency.addEmployee(new Surgeon("Inco", "Gnito", "4"));
        emergency.addEmployee(new Nurse("Nina", "Teknologi", "5"));
        emergency.addEmployee(new Nurse("Ove", "Ralt", "6"));
        emergency.addPatient(new Patient("Inga", "Lykke", "7", ""));
        emergency.addPatient(new Patient("Ulrik", "Smål", "8", ""));
        hospital.addDepartment(emergency);

        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.addEmployee(new Employee("Salti", "Kaffen", "9"));
        childrenPolyclinic.addEmployee(new Employee("Nidel V.", "Elvefølger", "10"));
        childrenPolyclinic.addEmployee(new Employee("Anton", "Nym", "11"));
        childrenPolyclinic.addEmployee(new GeneralPractitioner("Gene", "Sis", "12"));
        childrenPolyclinic.addEmployee(new Surgeon("Nanna", "Na", "13"));
        childrenPolyclinic.addEmployee(new Nurse("Nora", "Toriet", "14"));
        childrenPolyclinic.addPatient(new Patient("Hans", "Omvar", "15", ""));
        childrenPolyclinic.addPatient(new Patient("Laila", "La", "16", ""));
        childrenPolyclinic.addPatient(new Patient("Jøran", "Drebli", "17", ""));
        hospital.addDepartment(childrenPolyclinic);
        return hospital;
    }
}
